import React from "react";
import { Route } from "react-router-dom";
import App from "../App";
import PhotoContainer from "../containers/PhotoContainer";
import Main from "../components/Main";
import Audio from "../components/Audio";

const Routes = () => (
    <App>
        <Route 
          path="/" 
          component={Main} 
          exact>
        </Route>
        <Route 
          path="/photos" 
          component={PhotoContainer} 
          exact>
        </Route>
        <Route 
          path="/audios" 
          component={Audio} 
          exact>
        </Route>
    </App>
);

export default Routes

