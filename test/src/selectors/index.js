import { createStructuredSelector } from "reselect";

export const modelSelector = createStructuredSelector({
  photos: state => state.getPhotosReducer.photoList,
  error: state => state.getPhotosReducer.error,
  loading: state => state.getPhotosReducer.loading
});