import { applyMiddleware, createStore as createReduxStore } from "redux";
import createSagaMiddleware from "redux-saga";
import { routerMiddleware } from "react-router-redux";
import { createLogger } from "redux-logger";
import createHistory from "history/createBrowserHistory";


import reducer from "../reducers";
import mainSaga from "./saga";

const createStore = () => {
    const history = createHistory();
    const saga = createSagaMiddleware();

    const middleware = [routerMiddleware(history), saga];
    const logger = createLogger();    
    middleware.push(logger);

    const store = createReduxStore(reducer, applyMiddleware(...middleware));

    saga.run(mainSaga);

    return {
        store,
        history
    };
};

export default createStore;
