import { fork } from "redux-saga/effects";

import { saga as getPhotoSaga } from "../sagas";

export default function* mainSaga() {
    yield fork(getPhotoSaga);
}