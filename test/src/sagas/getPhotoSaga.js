import { put, takeLatest } from "redux-saga/effects";

import { getPhotosList } from "../api/requestAPI";
import * as actions from "../actions";

function* getPhotosSaga() {
  try {
    const result = yield getPhotosList();
    console.log("result", result);
    yield put(actions.getPhotosSuccess(result));
  } catch (e) {
    yield put(actions.getPhotosFailure(e.message));
  }
}

export default function*() {
  yield takeLatest(actions.GET_PHOTOS_REQUEST, getPhotosSaga);
}
