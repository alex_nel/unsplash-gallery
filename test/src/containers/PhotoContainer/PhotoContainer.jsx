import React, { Component,Fragment } from 'react';
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { containerActions } from "../../actions";
import { modelSelector } from "../../selectors";
import Photo from "../../components/Photo";

class PhotoContainer extends Component {
    static propTypes = {
        getPhotosRequest: PropTypes.func,
        photos: PropTypes.array,
    };

    componentDidMount() {
        this.props.getPhotosRequest();
    }
    
    renderPhoto() {
        const { photos } = this.props;
        return (
            <div className="gallery">
                {photos.map((item, index) => 
                    <Photo onShowWindow={this.handleShowWindow} key={index} data={item} />
                )}
            </div>
        )
        
    }

    render() {
        return(
            <Fragment>
                <div>
                    {this.renderPhoto()}
                </div>
            </Fragment>
            
        )
    }
}

export default connect(modelSelector, containerActions)(PhotoContainer);