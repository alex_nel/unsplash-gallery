import "isomorphic-fetch";

function status(res) {
  
  if (res.status === 401) {
    return Promise.reject(
      "Сессия истекла. Пожалуйста войдите и повторите это действие."
    );
  }

  return Promise.resolve(res);
}

export default function makeRequest(url, method, options = {}) {
  let fetchOptions = {
    method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json; charset=UTF-8;"
    }
  };

  fetchOptions = Object.assign(fetchOptions, options);

  return fetch(url, fetchOptions)
    .then(status)
    .then(res => res.json())
    .then(data => {
      return data;
    })
    .catch(err => {
      throw new Error(err);
    });
}
