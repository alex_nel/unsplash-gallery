import * as types from "../actions";

const initialState = {
  loading: false,
  error: "",
  photoList: []
};

export default function getPhotosReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case types.GET_PHOTOS_REQUEST:
      return { ...state, loading: true };

    case types.GET_PHOTOS_SUCCESS:
      return {
        ...state,
        photoList: [ ...payload.photoList],
        loading: false
      };

    case types.GET_PHOTOS_FAILURE:
      return { ...state, error: payload.message, loading: false };

    default:
      return state;
  }
}