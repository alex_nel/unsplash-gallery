import { combineReducers } from "redux";
import getPhotosReducer from "./getPhotosReducer";

const rootReducer = combineReducers({
    getPhotosReducer
});

export default rootReducer;