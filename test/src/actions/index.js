import { bindActionCreators } from "redux";

export const GET_PHOTOS_REQUEST = "GET_PHOTOS_REQUEST";
export const GET_PHOTOS_SUCCESS = "GET_PHOTOS_SUCCESS";
export const GET_PHOTOS_FAILURE = "GET_PHOTOS_FAILURE";


function getPhotosRequest() {
    return {
      type: GET_PHOTOS_REQUEST
    }
};

function getPhotosSuccess(photoList) {
    return {
      type: GET_PHOTOS_SUCCESS,
      payload: { photoList }
    }
};

function getPhotosFailure(message) {
    return {
      type: GET_PHOTOS_FAILURE,
      payload: message
    }
};

function containerActions(dispatch) {
    return bindActionCreators(
      {
        getPhotosRequest,
        getPhotosSuccess,
        getPhotosFailure
      },
      dispatch
    );
  }
  
  export {
    containerActions,

    getPhotosRequest,
    getPhotosSuccess,
    getPhotosFailure
  };

