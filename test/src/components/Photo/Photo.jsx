import React, { Component, Fragment } from 'react';
import PropTypes from "prop-types";

class Photo extends Component {
    static propTypes = {
        data: PropTypes.object
    };

    state = {
        showWindow: false
    }
    
    handleCloseWindow = evt => {
        const { showWindow } = this.state;
        evt.preventDefault();
        evt.stopPropagation();

        if (
            showWindow &&
            evt.target.className !== "full_photo" 
        ) {
            this.setState({ showWindow: false });
        }
    };

    handleShowWindow = () => {
        this.setState({showWindow: true})
    }

    renderPhotoWindow() {
        const { urls } = this.props.data;
        if(this.state.showWindow) {
            return(
                <div onClick={this.handleCloseWindow} className="photo_window">
                    <div className="full_photo">
                        <img src={urls.thumb} alt="" />
                    </div>
                </div>
            )
        }
    }

    render() {
        const { urls, description, user } = this.props.data
        return(
            <Fragment>
                <div className="photo_wrapper">
                    <div className="photo">
                        <img onClick={this.handleShowWindow} src={urls.thumb} alt="" />
                    </div>
                    <div className="descr">
                        <span>{description}</span>
                        <span>{user.name}</span>
                    </div>
                </div>
                {this.renderPhotoWindow()}
            </Fragment>
        )
    }
}

export default Photo;