import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'


class Main extends Component {
  render() {
    return (
      <div className="App">
        <h2>You can</h2>
        <button className="button"><NavLink to='/photos'>See photos</NavLink></button>
        <button className="button"><NavLink to='/audios'>Listen audios</NavLink></button>
      </div>
    );
  }
}

export default Main;
