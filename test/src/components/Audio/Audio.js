import React, { Component } from 'react';
import ReactAudioPlayer from 'react-audio-player';
import enter_sandman from '../../media/enter_sandman.mp3';
import mama_said from '../../media/mama_said.mp3';
import memory_remains from '../../media/memory_remains.mp3';
import nothing_else from '../../media/nothing_else.mp3';

class Audio extends Component {
  render() {
    return (
      <div className="App">
        <div className="audio_wrap">
          <div className='_audio'>
            <span>Enter sandman</span>  
            <ReactAudioPlayer
              src={enter_sandman}
              controls
            />
          </div>
          <div className='_audio'>
            <span> Mama said</span>  
            <ReactAudioPlayer
              src={mama_said}
              controls
            />
          </div>
          <div className='_audio'>
            <span>Memory remains</span>  
            <ReactAudioPlayer
              src={memory_remains}
              controls
            />
          </div>
          <div className='_audio'>
            <span> Nothing else matters</span>  
            <ReactAudioPlayer
              src={nothing_else}
              controls
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Audio;
