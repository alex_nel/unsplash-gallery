import React from 'react';
import { render } from "react-dom";
import './index.css';
import Routes from "./routes/index";
import { BrowserRouter as Router } from "react-router-dom";
import createStore from "./store/createStore";
import { Provider } from "react-redux";
import registerServiceWorker from './registerServiceWorker';

const create = createStore();
const store = create.store;

render(
    <Provider store={store}>
        <Router>
            <Routes />
        </Router>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
